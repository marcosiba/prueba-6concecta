<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Conciertos;
use App\Http\Traits\RentabilidadTrait;



class BeneficioTest extends TestCase
{
    use RentabilidadTrait;

     public function testRentabilidad()
     {
        $concierto = new Conciertos();
        $concierto->nombre ="Nombre del conierto";
        $concierto->fecha = "11/29/2022";
        $concierto->numero_espectadores = 8999;
        $concierto->promotor_id = 8;
        $concierto->recinto_id = 5;
        $concierto->save();
        $this->assertEquals(356889, $this->RentabilidadTrait($concierto->id));
     }

}
