<?php
namespace App\Http\Traits;
use App\Models\Recintos;
use App\Models\Grupos_Conciertos;
use App\Models\Grupos;
use App\Models\Conciertos;


trait RentabilidadTrait
{
    public function rentabilidad($id_concierto)
    {
        $coste = Recintos::where('id', $id_concierto)->first()->coste_alquiler;
        $grupos_conciertos = Grupos_Conciertos::where('concierto_id', $id_concierto)->get();

        $coste_entrada = Recintos::where('id', $id_concierto)->first()->precio_entrada;

        for($i = 0; $i < count($grupos_conciertos); $i++)
        {
            $coste += Grupos::where('id', $grupos_conciertos[$i]->grupo_id)
                ->first()->cache + 0.9 * $coste_entrada;
        }

        $beneficio = Conciertos::where('id', $id_concierto)->first()
            ->numero_espectadores * $coste_entrada;

        $beneficiototal = $beneficio - $coste;

        return $beneficiototal;


    }
}
