<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Conciertos;
use App\Models\Recintos;
use App\Models\Promotores;
use App\Models\Grupos;
use App\Models\Medios;
use App\Models\Grupos_Conciertos;
use App\Models\Medios_Coniertos;
use App\Http\Traits\RentabilidadTrait;

class ConciertoController extends Controller
{
    use RentabilidadTrait;

    public function index()
    {
        $recintos = Recintos::all();
        $promotores = Promotores::all();
        $grupos = Grupos::all();
        $medios = Medios::all();
        $recintos = Recintos::all();
        return view('index', compact('recintos', 'promotores', 'grupos', 'medios', 'recintos'));
    }

    //Guarda todos los grupos de un concierto
    private function saveGrupoConcierto($grupo, $idConcierto)
    {
        for($i = 0; $i < count($grupo); $i++)
        {
            $grupos_Conciertos = new Grupos_Conciertos();
            $grupos_Conciertos->grupo_id = $grupo[$i];
            $grupos_Conciertos->concierto_id = $idConcierto;
            $grupos_Conciertos->save();
        }
    }

    //Guarda todos los meidos de un concierto
    private function saveMediosConcierto($medio, $idConcierto)
    {
        for($i = 0; $i < count($medio); $i++)
        {
            $medios_Coniertos = new Medios_Coniertos();
            $medios_Coniertos->medio_id = $medio[$i];
            $medios_Coniertos->concierto_id = $idConcierto;
            $medios_Coniertos->save();
        }
    }

    public function store(Request $request)
    {
        $concierto = new Conciertos();
        $concierto->nombre = $request->nombre;
        $concierto->fecha = $request->fecha;
        $concierto->numero_espectadores = $request->numero_espectadores;
        $concierto->promotor_id = $request->promotor_id;
        $concierto->recinto_id = $request->recinto_id;
        $concierto->save();

        $this->saveGrupoConcierto($request->grupo, $concierto->id);
        $this->saveMediosConcierto($request->medio, $concierto->id);


        $concierto->rentabilidad = $this->rentabilidad($concierto->id);
        $concierto->update();

        $this->enviarMensaje($concierto->rentabilidad, $concierto->promotor_id);

        return redirect()->back()->with('message', 'Concierto guardado correctamente');

    }

    private function enviarMensaje($beneficio, $promotor_id)
    {

        $promotor_email = Promotores::where('id', $promotor_id)->first()->email;

        $data = [
                    "email" => $promotor_email,
                    "beneficio" => $beneficio
                ];

        \Mail::send('mail', ['data' => $data],
        function ($message) use ($data)
        {
            $message
                ->from("marcos.ib.inf.ing@gmail.com")
                ->to($data['email'])->subject($data['beneficio']);
            });
;
    }


}
