<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medios extends Model
{
    use HasFactory;

    public function conciertos()
    {
       return $this->belongsToMany(Conciertos::class, 'concierto_id');
    }
}
