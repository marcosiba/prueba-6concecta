<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grupos extends Model
{
    use HasFactory;

    public function Coniertos()
    {
        return $this->belongsToMany(Conciertos::class, 'concierto_id');
    }

}
