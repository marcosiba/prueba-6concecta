<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conciertos extends Model
{
    use HasFactory;

    public function ConciertoRecinto()
    {
       return $this->belongsTo(Recintos::class, 'recinto_id');
    }

    public function ConciertoPromotres()
    {
       return $this->belongsTo(Promotores::class, 'Promotor_id');
    }

    public function Coniertos()
    {
        return $this->belongsToMany(Grupo::class, 'grupo_id');
    }

    public function Medios()
    {
        return $this->belongsToMany(Medios::class, 'medio_id');
    }
}
