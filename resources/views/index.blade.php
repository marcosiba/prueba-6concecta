

@extends('template.template')

@section('content')
    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">
                <b>Alta Concierto </b>
            </h3>
        </div>
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <!--begin::Form-->
        <form class="form" method="POST" action="/store">
            @csrf
            <div class="card-body">
                <div class="form-group row">

                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <div class="input-group date mb-2">
                            <input type="text" class="form-control" placeholder="Nombre del concierto"
                                name="nombre" id="nombre" />
                            <div class="input-group-append">
                                <span class="input-group-text">

                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <div class="input-group date" >
                            <div class="input-group date" data-provide="datepicker" >
                                <input type="text" class="form-control" name="fecha" placeholder="Fecha">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <div class="input-group date mb-2">
                            <input type="number" class="form-control" placeholder="Número de espectadores" required 
                                name="numero_espectadores" />
                            <div class="input-group-append">
                                <span class="input-group-text">

                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <div class="form-group">
                            <select class="form-control" name="promotor_id">
                                <option selected disabled>Promotor</option>
                                @foreach ($promotores as $promotor)
                                    <option name="promotor_id" value="{{ $promotor->id }}">
                                        {{ $promotor->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <div class="form-group">
                            <select class="form-control" name="recinto_id">
                                <option selected disabled>Recintos</option>
                                @foreach ($promotores as $recinto)
                                    <option name="recinto_id" value="{{ $recinto->id }}">
                                        {{ $recinto->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <div class="form-group">
                            <select class="form-control" name="grupo[]" multiple>
                                <option selected disabled>Grupos</option>
                                @foreach ($grupos as $grupo)
                                    <option name="grupo[]" value="{{ $grupo->id }}">
                                        {{ $grupo->id }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <div class="form-group">
                            <select class="form-control" name="medio[]" multiple>
                                <option selected disabled>Medios</option>
                                @foreach ($medios as $medio)
                                    <option name="medio[]" value="{{ $medio->id }}">
                                        {{ $medio->id }}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
            </div>
            <div class="card-footer">
                <div class="form-group row">
                    <div class="col-lg-9 ml-lg-auto">
                        <button type="sumbit" class="btn btn-primary mr-2">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
            <!--end::Form-->
    </div>


@endsection


