<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medios__coniertos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('medio_id')->references('id')->on('medios');
            $table->foreignId('concierto_id')->references('id')->on('conciertos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medios__coniertos');
    }
};
