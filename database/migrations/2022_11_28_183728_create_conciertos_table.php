<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conciertos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->integer('numero_espectadores');
            $table->date('fecha');
            $table->integer('rentabilidad')->nullable();
            $table->foreignId('promotor_id')->references('id')->on('promotores');
            $table->foreignId('recinto_id')->references('id')->on('recintos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conciertos');
    }
};
