<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->date('fecha');
            $table->integer('espectadores');
            $table->double('rentabilidad');
            $table->foreignId('concierto_id')->references('id')->on('conciertos');
            $table->foreignId('provincia_id')->references('id')->on('provincias');
            $table->foreignId('recinto_id')->references('id')->on('recintos');
            $table->foreignId('promotor_id')->references('id')->on('promotores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
};
