<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Medios;
use Illuminate\Support\Facades\Schema;
use Faker\Factory as Faker;

class MediosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Medios::truncate();
        Schema::enableForeignKeyConstraints();

        $this->faker = Faker::create();

        for($i = 0; $i < 100; $i++)
        {
            $medios = new Medios();
            $medios->nombre = $this->faker->username;
            $medios->save();
        }
    }
}
