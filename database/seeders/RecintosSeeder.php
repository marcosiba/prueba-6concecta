<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Recintos;
use Illuminate\Support\Facades\Schema;
use Faker\Factory as Faker;

class RecintosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Recintos::truncate();
        Schema::enableForeignKeyConstraints();

        $faker = Faker::create();

        for($i = 0; $i < 100; $i++)
        {
            $recintos = new Recintos();
            $recintos->nombre = $faker->username;
            $recintos->coste_alquiler = $faker->numerify('#####');
            $recintos->precio_entrada = $faker->numerify('###');
            $recintos->save();
        }
    }
}
