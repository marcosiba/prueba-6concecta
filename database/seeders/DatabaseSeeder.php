<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PromotoresSeeder::class);
        $this->call(RecintosSeeder::class);
        $this->call(GruposSeeder::class);
        $this->call(MediosSeeder::class);
        $this->call(ProvinciaSeeder::class);
    }
}
