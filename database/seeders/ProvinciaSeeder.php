<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Provincia;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ProvinciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Provincia::truncate();
        Schema::enableForeignKeyConstraints();

        DB::table('provincias')->insert([
            ['id' => '1',  'provincia' => "Andalucía", 'created_at' => new \DateTime, 'updated_at' => new \DateTime],
            ['id' => '2',  'provincia' => "Aragón", 'created_at' => new \DateTime, 'updated_at' => new \DateTime],
            ['id' => '3',  'provincia' => "Asturias, Principado de", 'created_at' => new \DateTime, 'updated_at' => new \DateTime],
            ['id' => '4',  'provincia' => "Baleares, Islas", 'created_at' => new \DateTime, 'updated_at' => new \DateTime],
            ['id' => '5',  'provincia' => "Canarias", 'created_at' => new \DateTime, 'updated_at' => new \DateTime],
            ['id' => '6',  'provincia' => "Cantabria", 'created_at' => new \DateTime, 'updated_at' => new \DateTime],
            ['id' => '7',  'provincia' => "Castilla y León", 'created_at' => new \DateTime, 'updated_at' => new \DateTime],
            ['id' => '8',  'provincia' => "Castilla - La Mancha", 'created_at' => new \DateTime, 'updated_at' => new \DateTime],
            ['id' => '9',  'provincia' => "Cataluña", 'created_at' => new \DateTime, 'updated_at' => new \DateTime],
            ['id' => '10',  'provincia' => "Comunidad Valenciana", 'created_at' => new \DateTime, 'updated_at' => new \DateTime],
            ['id' => '11',  'provincia' => "Extramadura", 'created_at' => new \DateTime, 'updated_at' => new \DateTime],
            ['id' => '12',  'provincia' => "Galicia", 'created_at' => new \DateTime, 'updated_at' => new \DateTime],
            ['id' => '13',  'provincia' => "Madrid, Comunidad de", 'created_at' => new \DateTime, 'updated_at' => new \DateTime],
            ['id' => '14',  'provincia' => "Murcia, Región de", 'created_at' => new \DateTime, 'updated_at' => new \DateTime],
            ['id' => '15',  'provincia' => "Navarra, Comunidad Foral de", 'created_at' => new \DateTime, 'updated_at' => new \DateTime],
            ['id' => '16',  'provincia' => "País Vasco", 'created_at' => new \DateTime, 'updated_at' => new \DateTime],
            ['id' => '17',  'provincia' => "Rioja, La", 'created_at' => new \DateTime, 'updated_at' => new \DateTime],
            ['id' => '18',  'provincia' => "Ceuta", 'created_at' => new \DateTime, 'updated_at' => new \DateTime],
            ['id' => '19',  'provincia' => "Melilla", 'created_at' => new \DateTime, 'updated_at' => new \DateTime]
        ]);
    }
}

