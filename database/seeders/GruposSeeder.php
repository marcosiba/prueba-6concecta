<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Grupos;
use Illuminate\Support\Facades\Schema;
use Faker\Factory as Faker;
class GruposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Grupos::truncate();
        Schema::enableForeignKeyConstraints();

        $faker = Faker::create();

        for($i = 0; $i < 100; $i++)
        {
            $grupos = new Grupos();
            $grupos->nombre = $faker->username;
            $grupos->cache = $faker->numerify('####');
            $grupos->save();
        }
    }
}
