<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Promotores;
use Illuminate\Support\Facades\Schema;
use Faker\Factory as Faker;

class PromotoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Promotores::truncate();
        Schema::enableForeignKeyConstraints();

        $this->faker = Faker::create();

        for($i = 0; $i < 100; $i++)
        {
            $promotores = new Promotores();
            $promotores->email = $this->faker->email;
            $promotores->nombre = $this->faker->username;
            $promotores->save();
        }
    }
}


